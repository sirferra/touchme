# TouchMe (Traducción Ordinaria de linUx del arCHivo de procoM al rEmmina)

Lo que hace este script, es leer el archivo de conexiones de procom (rdc)<br>
y generarte los archivos necesarios para que puedas conectarte a los servidores<br>
<br>
**Para esto, necesitás un archivo que te pasa procom. <br>
El archivo es DataCenter-Procom.rdg, pegalo dentro de la carpeta con este nombre.**
<br>
<br>
<br>
**Instrucciones para usuarios de linux**<br>
Clonás el repo, dentro del repo instalá las dependencias de node `npm install`<br>
Ejecutá touchme con el siguiente comando `node touchme.js usuario.procom`<br>
Donde usuario procom, es tu usuario procom :D <br>
En algunos servers no te va a funcionar ese usuario, usá flexxus y la clave genérica.

**Instrucciones for dummies**<br>
*  Instalar git
*  Clonar el repositorio
*  Instalar npm
*  Instalar las dependencias
*  Instalar remmina
*  Correr touchme

<br>
Updateas tus respositorios<br>

`sudo apt-get update`

Instalás git<br>

`sudo apt-get install git`

Te dirigís a donde querés bajarlo, como ejemplo te pongo Descargas (el ~ representa tu /home/usuario/)<br>
`cd ~/Descargas`

Clonas el repositorio de la siguiente forma (clonarlo es decargarlo, btw)<br>
`git clone https://gitlab.com/flexxus/touchme.git`

Instalas npm. <br>
`sudo apt-get install npm`<br>

Te metés al directorio <br>
`cd touchme`<br>

Instalás las dependencias(te pongo sudo por las dudas)<br>
`sudo npm install`

Si no tenés Remmina, instalalo. Te dejo el comando, pero de última buscalo si no funciiona.<br>
`sudo apt-get install remmina`<br>

Una vez hecho esto corrés touchme<br>
`node touchme.js usuario.procom`<br>

Donde usuario.procom, es el usuario que te proporcionó Procom ;D.<br>
Aveces no te va a funcionar en algunas conexiones, usá el de flexxus.<br>
<br>
