let xmlParser = require('xml2json');
let Promise = require('bluebird');
let fs = Promise.promisifyAll(require('fs'));
var content;
fs.existsAsync = function(path){
    return fs.openAsync(path, "r").then(function(stats){
      return true
    }).catch(function(stats){
      return false
    })
  }
fs.existsAsync("./DataCenter-Procom.rdg").then(function(exists){
    if(!exists){
        console.error('Pone el archivo de "DataCenter-Procom.rdg" al lado de touchme.js')
    }else{
        content = fs.readFileSync('./DataCenter-Procom.rdg','utf8');
        jsoned = xmlParser.toJson(content,{
            object:true,
        });
        const cabecera = '[remmina]';
        if(process.argv[2] == undefined){
            console.error('Tenés que pasarle el nombre de usuario como argumento:\nnode touchme.js usuario.procom')
        }else{
            fs.mkdir('remmina',(e)=>{});
            fs.mkdir(process.env['HOME'] + '/.local/share/remmina/',e=>{});
            fs.mkdir(process.env['HOME'] + '/.remmina/',e=>{});
            const username = 'username='+ process.argv[2];

            /* Si te das mania, podés cambiarte algunas opciones de acá :3
            sharefolder, podés poner una carpeta que hayas creado para compartir con el server.
            Tené cuidado, porque esta carpeta se va a montar en el servidor mientras abras la conexión. 
            */
            const file = 
            `sharesmartcard=0
            resolution_width=1400
            resolution_height=1050
            sharefolder=
            disableclipboard=0
            shareprinter=0
            microphone=0
            gateway_server=
            sound=local
            protocol=RDP
            keyboard_grab=0
            gateway_usage=0
            colordepth=32
            execpath=
            authentication level=0
            exec=
            password=
            gateway_password=
            window_maximize=1
            viewmode=4
            gateway_username=
            ssh_privatekey=
            serialname=
            enableproxy=0
            ssh_password=
            ssh_auth=0
            console=0
            security=
            precommand=
            disable_fastpath=0
            postcommand=
            ssh_charset=
            group=procom
            ssh_enabled=0
            glyph-cache=0
            printerdriver=
            parallelpath=
            cert_ignore=0
            serialpermissive=0
            printername=
            resolution_mode=0
            disableautoreconnect=0
            loadbalanceinfo=
            clientname=
            ssh_username=
            relax-order-checks=0
            gateway_domain=
            serialdriver=
            domain=procom
            smartcardname=
            serialpath=
            ssh_loopback=0
            disablepasswordstoring=0
            quality=0
            shareparallel=0
            parallelname=
            shareserial=0
            gwtransp=http
            ssh_server=
            last_success=20190926
            window_height=456
            window_width=1401
            scale=1`;
            var servers = jsoned.RDCMan.file.group;
            servers.forEach(server => {
                server.server.forEach(item=>{
                    var name = (item.properties.displayName);
                    var ip = (item.properties.name);
                    var data = cabecera + "\nname=" + name + "\nserver=" + ip+'\n'+username+'\n';
                    data+= file;
                    fs.writeFileSync('remmina/'+name+'.remmina',data,e=>{});
                    fs.copyFileSync('remmina/'+name+'.remmina',process.env['HOME'] + '/.remmina/'+name+'.remmina')
                    fs.renameSync('remmina/'+name+'.remmina', process.env['HOME'] + '/.local/share/remmina/'+name+'.remmina',(e)=>{
                        if(e)console.log(e);
                    });
                })
            });
        }
        console.log("En caso de que no se hayan copiado, copìá los archivos de la carpeta remmina en ~/.local/share/remmina/")
    }
})
